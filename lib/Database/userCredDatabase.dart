import 'package:flutter/material.dart';
import 'package:fluttertestapp/Model/loginModel.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:path/path.dart';
import 'package:sqflite/sqflite.dart';

class UserDatabase {
  static final UserDatabase instance = UserDatabase._init();

  static Database? _database;

  UserDatabase._init();

  Future<Database> get database async {
    if (_database != null) return _database!;

    _database = await _initDB('user.db');
    return _database!;
  }

  Future<Database> _initDB(String filePath) async {
    final dbPath = await getDatabasesPath();
    final path = join(dbPath, filePath);

    return await openDatabase(path, version: 2, onCreate: _createDB);
  }

  Future _createDB(Database db, int version) async {
    final idType = 'INTEGER PRIMARY KEY AUTOINCREMENT';
    final textType = 'TEXT NOT NULL';
    final integerType = 'INTEGER NOT NULL';

    await db.execute('''
CREATE TABLE $tableUser ( 
  ${UserCredFields.id} $idType, 
    ${UserCredFields.name} $textType,
  ${UserCredFields.email} $textType,
  ${UserCredFields.phone} $integerType,
  ${UserCredFields.pass} $textType,
  ${UserCredFields.image} $textType
  )
''');
  }

  Future<User> create(User user) async {
    final db = await instance.database;
    final id = await db.insert(tableUser, user.toJson());
    print(user.copy(id: id));
    return user.copy(id: id);
  }

  Future<User> readUser(int id) async {
    final db = await instance.database;

    final maps = await db.query(
      tableUser,
      columns: UserCredFields.values,
      where: '${UserCredFields.id} = ?',
      whereArgs: [id],
    );

    if (maps.isNotEmpty) {
      return User.fromJson(maps.first);
    } else {
      throw Exception('ID $id not found');
    }
  }

  // Future<List<User>> readAllNotes() async {
  //   final db = await instance.database;
  //
  //  // final orderBy = '${UserCredFields.time} ASC';
  //   // final result =
  //   //     await db.rawQuery('SELECT * FROM $tableNotes ORDER BY $orderBy');
  //
  //   final result = await db.query(tableUser, );
  //
  //   return result.map((json) => User.fromJson(json)).toList();
  // }

  Future<int> update(User user) async {
    final db = await instance.database;

    return db.update(
      tableUser,
      user.toJson(),
      where: '${UserCredFields.id} = ?',
      whereArgs: [user.id],
    );
  }

  Future<int> delete(int id) async {
    final db = await instance.database;

    return await db.delete(
      tableUser,
      where: '${UserCredFields.id} = ?',
      whereArgs: [id],
    );
  }

  Future close() async {
    final db = await instance.database;

    db.close();
  }


  Future<User?> getLogin(String user, String password) async {
    final db = await instance.database;
    var res = await db.rawQuery("SELECT * FROM user WHERE email = '$user' and pass = '$password'");
print(res);
     if (res == []) {
       Fluttertoast.showToast(
                   msg: "Your email or your password is not matching",
                   toastLength: Toast.LENGTH_SHORT,
                   gravity: ToastGravity.CENTER,
                   timeInSecForIosWeb: 1,
                   backgroundColor: Colors.red,
                   textColor: Colors.white,
                   fontSize: 16.0
               );
     }


    return null;
  }
}