
import 'dart:io';

import 'package:flutter/material.dart';
import 'package:fluttertestapp/Database/userCredDatabase.dart';
import 'package:fluttertestapp/Model/loginModel.dart';
import 'package:fluttertestapp/Widgets/roundedButton.dart';
import 'package:fluttertestapp/utility.dart';
import 'package:geolocator/geolocator.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';

import 'AuthorList.dart';

class HomeScreen extends StatefulWidget {
  @override
  _HomeScreenState createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  late GoogleMapController mapController;
  LatLng? currentPostion;
  late User user;
  bool isLoading = false;
String name = "";
File? image ;
  void _onMapCreated(GoogleMapController controller) {
    mapController = controller;
  }


  Set<Marker> _createMarker() {
    return <Marker>[
      Marker(
        markerId: MarkerId('currentLocation'),
        position: currentPostion!,
        icon: BitmapDescriptor.defaultMarker,
        infoWindow: InfoWindow(title: "Current Location"),
      ),
    ].toSet();
  }

  void _getUserLocation() async {
    var position = await GeolocatorPlatform.instance
        .getCurrentPosition(desiredAccuracy: LocationAccuracy.high);

    setState(() {
      currentPostion = LatLng(position.latitude, position.longitude);
    });
  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    _getUserLocation();
    getUserData();
  }

  @override
  Widget build(BuildContext context) {
    var height = MediaQuery.of(context).size.height;

    return Scaffold(
        appBar: AppBar(
          title: Text(name),
         //  actions: <Widget>[Image.file(image! )],
        ),
        body: SingleChildScrollView(child:Column(
          children: [
            Container(
              height: height/1.5,
              child: currentPostion == null
                  ? Container(
                      child: Center(
                        child: Text(
                          'loading map..',
                          style: TextStyle(
                              fontFamily: 'Avenir-Medium',
                              color: Colors.grey[400]),
                        ),
                      ),
                    )
                  : GoogleMap(
                      markers: _createMarker(),
                      onMapCreated: _onMapCreated,
                      initialCameraPosition: CameraPosition(
                        target: currentPostion!,
                        zoom: 11.0,
                      ),
                    ),
            ),

            RoundedButton(
              text: "Go To next page",
              press: () {
                Navigator.push(
                  context,
                  MaterialPageRoute(builder: (context) => AuthorList()),
                );
                getUserData();
              },
            ), ],
        ),

    ),);
  }

  Future getUserData() async {

    this.user = await UserDatabase.instance.readUser(1);

    setState(() {
      isLoading = false;
      //Utility.imageFromBase64String(user.image);
name = user.name;
    });

    print("user = ${user.image}");
  }


}
