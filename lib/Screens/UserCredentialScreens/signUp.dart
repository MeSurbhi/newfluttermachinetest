import 'dart:io';

import 'package:flutter/material.dart';
import 'package:fluttertestapp/Database/userCredDatabase.dart';
import 'package:fluttertestapp/Model/loginModel.dart';
import 'package:fluttertestapp/Utils/constant.dart';
import 'package:fluttertestapp/Widgets/roundedButton.dart';
import 'package:fluttertestapp/Widgets/textFieldContainer.dart';
import 'package:fluttertestapp/Widgets/textfieldWidget.dart';
import 'package:image_picker/image_picker.dart';

import '../../utility.dart';
import '../HomeScreen.dart';

class SignUpScreen extends StatefulWidget {
  @override
  _SignUpScreenState createState() => _SignUpScreenState();
}

class _SignUpScreenState extends State<SignUpScreen> {
String imgString ='';


  String email = "";
  String name = "";
  String pass = "";
  String mobile = "";
  XFile? _image;
  final ImagePicker _picker = ImagePicker();


  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return Scaffold(
      body: SafeArea(
        child: SingleChildScrollView(
          child: Center(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                SizedBox(height: size.height * 0.03),
                Text(
                  'Sign Up',
                  style: TextStyle(
                      fontSize: size.height * 0.03,
                      fontWeight: FontWeight.bold),
                ),
                Center(
                  child: GestureDetector(
                    onTap: () {
                      _showPicker(context);
                    },
                    child: CircleAvatar(
                      radius: 55,
                      backgroundColor: kPrimaryLightColor,
                      child: _image != null
                          ? ClipRRect(
                              borderRadius: BorderRadius.circular(50),
                              child: Image.file(
                                File(_image!.path),
                                width: 100,
                                height: 100,
                                fit: BoxFit.fitHeight,
                              ),
                            )
                          : Container(
                              decoration: BoxDecoration(
                                  color: Colors.grey[200],
                                  borderRadius: BorderRadius.circular(50)),
                              width: 100,
                              height: 100,
                              child: Icon(
                                Icons.camera_alt,
                                color: kPrimaryColor,
                              ),
                            ),
                    ),
                  ),
                ),
                SizedBox(height: size.height * 0.03),
                TextFieldContainer(
                  child: InputField(
                    hintText: "Name",
                    icon: Icons.person,
                    onChanged: (value) {
                      setState(() {
                        name = value;

                      });
                    },
                  ),
                ),
                TextFieldContainer(
                  child: InputField(
                    hintText: "Email",
                    icon: Icons.email,
                    onChanged: (value) {
                      setState(() {
                        email = value;

                      });
                    },
                  ),
                ),
                TextFieldContainer(
                  child: InputField(
                    hintText: "Password",
                    icon: Icons.enhanced_encryption_outlined,
                    onChanged: (value) {
                      setState(() {
                        pass = value;

                      });
                    },
                  ),
                ),
                TextFieldContainer(
                  child: InputField(
                    hintText: "Mobile",
                    icon: Icons.call,
                    onChanged: (value) {
                      setState(() {
                        mobile = value;
                      });
                    },
                  ),
                ),
                RoundedButton(
                  text: "Sign Up",
                  press: () {
                    add();

                  },
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }



  _imgFromCamera() async {

    final XFile? image =
        await _picker.pickImage(source: ImageSource.camera, imageQuality: 50);
    setState(() {

      _image = image;
    });
  }

  _imgFromGallery() async {
    final XFile? image =
    await _picker.pickImage(source: ImageSource.camera, imageQuality: 50);


    // final XFile? image =
    //     await _picker.pickImage(source: ImageSource.gallery, imageQuality: 50);
    //  imgString = Utility.base64String(File(image.toString()).readAsBytesSync());

    setState(() {
      _image = image ;
    });
  }

  void _showPicker(context) {
    showModalBottomSheet(
        context: context,
        builder: (BuildContext bc) {
          return SafeArea(
            child: Container(
              child: new Wrap(
                children: <Widget>[
                  new ListTile(
                      leading: new Icon(Icons.photo_library),
                      title: new Text('Photo Library'),
                      onTap: () {
                        _imgFromGallery();
                        Navigator.of(context).pop();
                      }),
                  new ListTile(
                    leading: new Icon(Icons.photo_camera),
                    title: new Text('Camera'),
                    onTap: () {
                      _imgFromCamera();
                      Navigator.of(context).pop();
                    },
                  ),
                ],
              ),
            ),
          );
        });
  }

  Future add() async {

    final user = User(
      name: name,
      email: email,
      phone: int.parse(mobile),
      pass: pass,
     // image: Utility.base64String(File(_image.toString()).readAsBytesSync()),
        image:_image.toString()
    );
    print(user);
    await UserDatabase.instance.create(user);
    Navigator.push(
      context,
      MaterialPageRoute(builder: (context) => HomeScreen()),
    );
  }


}
