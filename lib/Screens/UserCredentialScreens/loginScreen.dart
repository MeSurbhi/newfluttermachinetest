import 'package:flutter/material.dart';
import 'package:fluttertestapp/Database/userCredDatabase.dart';
import 'package:fluttertestapp/Screens/HomeScreen.dart';
import 'package:fluttertestapp/Screens/UserCredentialScreens/signUp.dart';
import 'package:fluttertestapp/Widgets/textfieldWidget.dart';
import 'package:fluttertoast/fluttertoast.dart';

import '../../Model/loginModel.dart';
import '../../Widgets/roundedButton.dart';
import '../../Widgets/textFieldContainer.dart';
import '../../Utils/constant.dart';

class LoginScreen extends StatefulWidget {
  @override
  _LoginScreenState createState() => _LoginScreenState();
}

class _LoginScreenState extends State<LoginScreen> {
  late User user;

  String email = "";
  final pass = TextEditingController();

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return Scaffold(
      body: SafeArea(
        child: SingleChildScrollView(
          child: Center(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                SizedBox(height: size.height * 0.03),
                Text(
                  'Login In',
                  style: TextStyle(
                      fontSize: size.height * 0.03,
                      fontWeight: FontWeight.bold),
                ),
                SizedBox(height: size.height * 0.03),
                TextFieldContainer(
                  child: InputField(
                    hintText: "Your Email",
                    icon: Icons.person,
                    onChanged: (value) {
                      setState(() {
                        email = value;
                      });
                    },
                  ),
                ),
                TextFieldContainer(
                  child: TextFormField(
                    cursorColor: kPrimaryColor,
                    controller: pass,
                    obscureText: true,
                    decoration: InputDecoration(
                      icon: Icon(
                        Icons.enhanced_encryption_outlined,
                        color: kPrimaryColor,
                      ),
                      hintText: "*********",
                      border: InputBorder.none,
                    ),
                  ),
                ),
                RoundedButton(
                  text: "Login In",
                  press: () {
                    logIn();
                    // UserDatabase.instance.getLogin(email, pass);
                    //  logIn();
                  },
                ),
                SizedBox(height: size.height * 0.03),
                GestureDetector(
                  onTap: () {
                    Navigator.push(
                      context,
                      MaterialPageRoute(builder: (context) => SignUpScreen()),
                    );
                  },
                  child: Text(
                    "Sign Up",
                    style: TextStyle(fontWeight: FontWeight.bold),
                  ),
                )
              ],
            ),
          ),
        ),
      ),
    );
  }

  void logIn() async {
    print('hhh');
    print(UserDatabase.instance);
    this.user = await UserDatabase.instance.readUser(1);
    print(email);
    print(pass);
    print(user.email);
    print(user.pass);
    if (email == user.email && pass.text == user.pass) {
      Navigator.push(
        context,
        MaterialPageRoute(builder: (context) => HomeScreen()),
      );
    } else {
      Fluttertoast.showToast(
          msg: "Your email or your password is not matching",
          toastLength: Toast.LENGTH_SHORT,
          gravity: ToastGravity.CENTER,
          timeInSecForIosWeb: 1,
          backgroundColor: Colors.red,
          textColor: Colors.white,
          fontSize: 16.0);
    }
  }
}
