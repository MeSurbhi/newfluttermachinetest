import 'dart:convert';
import 'dart:io';

import 'package:flutter/material.dart';
import 'package:flutter_staggered_grid_view/flutter_staggered_grid_view.dart';
import 'package:fluttertestapp/Model/authorModel.dart';
import 'package:http/http.dart' as http;

class AuthorList extends StatefulWidget {
  @override
  _AuthorListState createState() => _AuthorListState();
}

class _AuthorListState extends State<AuthorList> {
  List<AuthorModel> authorList = [];
  List<AuthorModel> authorListAdd = [];
  bool _loading = false;
  int pageNumber = 1;
  ScrollController _scrollController = ScrollController();


  Future<List<AuthorModel>> getData() async {
    String link = 'https://picsum.photos/v2/list?page=$pageNumber';
    print(link);
    try {
      var res = await http.get(
        Uri.parse(link),
        headers: {
          "Accept": "application/json",
        },
      );

      if (res.statusCode == 200) {
        var data = json.decode(res.body);
        print(data);
        var rest = data as List;
        setState(() {
          authorListAdd = rest
              .map<AuthorModel>((json) => AuthorModel.fromJson(json))
              .toList();
          authorList.addAll(authorListAdd);
          _loading = false;
        });
        print(authorList.length);
      }
    } on SocketException {}

    return authorList;
  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    getData();
    _scrollController.addListener(() {
      if (_scrollController.position.pixels ==
          _scrollController.position.maxScrollExtent) {
        pageNumber = pageNumber + 1;
        print("pageNumber");
        print(pageNumber);
        getData();
        setState(() {
          _loading = true;
        });
      }
    });}
  @override

  Widget build(BuildContext context) {
    var height = MediaQuery.of(context).size.height;
    var width = MediaQuery.of(context).size.width;
    return Scaffold(
      appBar: AppBar(
        title: Text("Author List"),
      ),
      body: ListView(children: [
          Container(
          height:height,
          width: 400,
          margin: EdgeInsets.symmetric(vertical: 10.0),

          child: (authorList.length == 0)
            ? Center(child:CircularProgressIndicator())
            : Column(
          children: <Widget>[


            Expanded(
                child:  StaggeredGridView.countBuilder(
                crossAxisCount: 2,
                crossAxisSpacing: height*0.010,
                mainAxisSpacing:height*0.010,
                itemCount: authorList.length,
                itemBuilder: (context, index) {
                  return  Container(
                      decoration: BoxDecoration(
                          shape: BoxShape.rectangle,
                          borderRadius: BorderRadius.all(
                              Radius.circular(15.0)),
                          border: Border.all(color: Colors.grey)),
                      child: Column(
                        children: <Widget>[
                          Container(
                            height: 150,
                            child: ClipRRect(
                              borderRadius: BorderRadius.all(
                                  Radius.circular(15)),
                              child: FadeInImage.assetNetwork(
                                placeholder:
                                'assets/images/logo.png',
                                image: authorList[index].downloadUrl?? "",
                                fit: BoxFit.cover,
                              ),
                            ),
                          ),
                          SizedBox(height: 10),
                          Text(
                            authorList[index].author?? "",
                            overflow: TextOverflow.ellipsis,
                          ),
                        ],
                      ),

                  );
                },
                staggeredTileBuilder: (index) {
                  return StaggeredTile.count(
                      1, index.isEven ? 1.1 : 1.1);
                }),),


          ],
        ),
      ),
        Container(
          height: _loading ? 50.0 : 0,
          color: Colors.transparent,
          child: Center(
            child: new CircularProgressIndicator(),
          ),
        ),
    ],),
    );
  }
}
