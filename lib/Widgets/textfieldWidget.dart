import 'package:flutter/material.dart';

import '../Utils/constant.dart';

class InputField extends StatelessWidget {
  final String? hintText;
  final IconData? icon;
  final TextEditingController? controller;
  final ValueChanged<String>? onChanged;
  const InputField({
    Key? key,
    this.hintText,
    this.icon,
    this.controller,
    this.onChanged,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return  TextField(
      controller: controller,
        onChanged: onChanged,
        cursorColor: kPrimaryColor,
        decoration: InputDecoration(
          icon: Icon(
            icon,
            color: kPrimaryColor,
          ),
          hintText: hintText,
          border: InputBorder.none,
        ),

    );
  }
}