import 'dart:typed_data';

final String tableUser = 'user';

class UserCredFields {
  static final List<String> values = [
    /// Add all fields
    id,name, email, phone, pass, image
  ];

  static final String id = '_id';
  static final String name = 'name';
  static final String email = 'email';
  static final String phone = 'phone';
  static final String pass = 'pass';
  static final String image = 'image';
}

class User {
  final int? id;
  final String name;
  final String email;
  final int phone;
  final String pass;
  final String image;

  const User({
    this.id,
    required this.name,
    required this.email,
    required this.phone,
    required this.pass,
    required this.image,
  });

  User copy({
    int? id,
    String? name,
    String? email,
    int? phone,
    String? pass,
    String? image,
  }) =>
      User(
        id: id ?? this.id,
          name: name ?? this.name,
        email: email ?? this.email,
        phone: phone ?? this.phone,
        pass: pass ?? this.pass,
        image : image ?? this.image
      );

  static User fromJson(Map<String, Object?> json) => User(
    id: json[UserCredFields.id] as int?,
    name: json[UserCredFields.name] as String,
    email: json[UserCredFields.email] as String,
    phone: json[UserCredFields.phone] as int,
    pass: json[UserCredFields.pass] as String,
    image: json[UserCredFields.image] as String,

  );

  Map<String, Object?> toJson() => {
    UserCredFields.id: id,
    UserCredFields.name: name,
    UserCredFields.email: email,
    UserCredFields.phone: phone,
    UserCredFields.pass: pass,
    UserCredFields.image: image,
  };
}